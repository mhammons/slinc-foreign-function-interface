package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{CLinker, GroupLayout}

import scala.jdk.OptionConverters.*
import scala.jdk.CollectionConverters.*
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodType

trait CLinker18 extends CLinkerProto, Descriptor18, Address18, Allocator18:
  type Linker = CLinker

  extension (l: Linker)
    def downcallHandle(
        symbol: ForeignSymbol,
        descriptor: Descriptor,
        allocator: Allocator
    ): MethodHandle =
      if descriptor.returnLayout.nn.toScala
          .map {
            case g: Layout.Group => true
            case _               => false
          }
          .getOrElse(false)
      then l.downcallHandle(symbol, descriptor).nn.bindTo(allocator).nn
      else l.downcallHandle(symbol, descriptor).nn

    def upcallStub(
        target: MethodHandle,
        descriptor: Descriptor,
        scope: Scope
    ): ForeignSymbol =
      l.upcallStub(target, descriptor, scope).nn

  val Linker = new LinkerSingleton:
    def systemCLinker(): Linker = CLinker.systemCLinker().nn
    private def layoutTypeToCarrierType(layout: Layout) = layout match
      case CChar          => classOf[Byte]
      case CInt           => classOf[Int]
      case CShort         => classOf[Short]
      case CLong          => classOf[Long]
      case CFloat         => classOf[Float]
      case CDouble        => classOf[Double]
      case CPointer       => classOf[Address]
      case g: GroupLayout => classOf[Segment]

    def downcallType(descriptor: Descriptor): MethodType = (
      descriptor.returnLayout.nn.toScala,
      descriptor.argumentLayouts.nn.asScala.toList
    ) match
      case (Some(ret), Nil) =>
        MethodType.methodType(layoutTypeToCarrierType(ret)).nn
      case (None, Nil) => VoidHelper.methodTypeV().nn
      case (Some(ret), args) =>
        MethodType
          .methodType(
            layoutTypeToCarrierType(ret),
            layoutTypeToCarrierType(args.head),
            args.tail.map(layoutTypeToCarrierType).toSeq*
          )
          .nn
      case (None, args) =>
        VoidHelper
          .methodTypeV(
            layoutTypeToCarrierType(args.head),
            args.tail.map(layoutTypeToCarrierType).toSeq*
          )
          .nn
    def upcallType(descriptor: Descriptor): MethodType = downcallType(
      descriptor
    )
