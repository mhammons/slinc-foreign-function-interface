package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{
  ValueLayout,
  MemoryLayout,
  GroupLayout,
  SequenceLayout
}
import scala.jdk.OptionConverters.*
import scala.jdk.CollectionConverters.*
import scala.reflect.TypeTest

trait Layout18 extends LayoutProto:
  type Layout = MemoryLayout

  given typeTestOfLayout: TypeTest[Layout, Layout] = TypeTest.identity[Layout]

  class Layout18Singleton extends LayoutSingleton:
    type Group = GroupLayout
    type Value = ValueLayout
    type Sequence = SequenceLayout

    given typeTestOfGroup: TypeTest[Layout, Group] = l =>
      l match 
        case g: (l.type & Group) => Some(g)
        case _ => None

    given typeTestOfValue: TypeTest[Layout, Value] = l => 
      l match 
        case v: (l.type & Value) => Some(v)
        case _ => None

    given typeTestOfSequence: TypeTest[Layout, Sequence] = l =>
      l match 
        case s: (l.type & Sequence) => Some(s)
        case _ => None

    type Path = MemoryLayout.PathElement

    class Path18Singleton extends PathSingleton:
      def groupElement(name: String): Path =
        MemoryLayout.PathElement.groupElement(name).nn
      def sequenceElement(index: Long): Path =
        MemoryLayout.PathElement.sequenceElement(index).nn

    val Path: Path18Singleton = new Path18Singleton

    def sequenceLayout(size: Long, layout: Layout): Sequence =
      MemoryLayout.sequenceLayout(size, layout).nn
    def structLayout(layouts: Layout*): Group =
      MemoryLayout.structLayout(layouts*).nn

  val Layout: Layout18Singleton = new Layout18Singleton

  type CChar = ValueLayout.OfByte
  type CDouble = ValueLayout.OfDouble
  type CFloat = ValueLayout.OfFloat
  type CInt = ValueLayout.OfInt
  type CLong = ValueLayout.OfLong
  type CPointer = ValueLayout.OfAddress
  type CShort = ValueLayout.OfShort

  given typeTestOfCChar: TypeTest[Layout.Value, CChar] = l =>
    l match 
      case c: (l.type & ValueLayout.OfByte) => Some(c)
      case _ => None

  given typeTestOfCDouble: TypeTest[Layout.Value, CDouble] = l =>
    l match 
      case d: (l.type & ValueLayout.OfDouble) => Some(d)
      case _ => None

  given typeTestOfCFloat: TypeTest[Layout.Value, CFloat] = l => 
    l match 
      case f: (l.type & ValueLayout.OfFloat) => Some(f)
      case _ => None

  given typeTestOfCInt: TypeTest[Layout.Value, CInt] = l =>
    l match 
      case i: (l.type & ValueLayout.OfInt) => Some(i)
      case _ => None

  given typeTestOfCLong: TypeTest[Layout.Value, CLong] = l =>
    l match
      case i: (l.type & ValueLayout.OfLong) => Some(i)
      case _ => None

  given typeTestOfCPointer: TypeTest[Layout.Value, CPointer] = l => 
    l match 
      case p: (l.type & ValueLayout.OfAddress) => Some(p)
      case _ => None

  given typeTestOfCShort: TypeTest[Layout.Value, CShort] = l => 
    l match 
      case s: (l.type & ValueLayout.OfShort) => Some(s)
      case _ => None

  val CChar: CChar = ValueLayout.JAVA_BYTE.nn
  val CDouble: CDouble = ValueLayout.JAVA_DOUBLE.nn
  val CFloat: CFloat = ValueLayout.JAVA_FLOAT.nn
  val CInt: CInt = ValueLayout.JAVA_INT.nn
  val CLong: CLong = ValueLayout.JAVA_LONG.nn
  val CPointer: CPointer = ValueLayout.ADDRESS.nn
  val CShort: CShort = ValueLayout.JAVA_SHORT.nn

  extension [A <: Layout](vl: A)
    def withName(name: String): A =
      vl.withName(name).asInstanceOf[A]
    def byteSize =
      vl.byteSize
    def byteOffset(pathElements: Layout.Path*): Long =
      vl.byteOffset(pathElements*).nn
    def name: Option[String] = vl.name.nn.toScala

  extension (gl: GroupLayout)
    def memberLayouts: List[Layout] = gl.memberLayouts.nn.asScala.toList
