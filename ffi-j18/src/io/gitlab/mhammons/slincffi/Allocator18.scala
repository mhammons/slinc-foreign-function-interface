package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.SegmentAllocator

trait Allocator18 extends AllocatorProto, Scope18, Address18, Layout18:
  type Allocator = SegmentAllocator

  val Allocator: AllocatorSingleton = new AllocatorSingleton:
    def arenaAllocator(size: Long, scope: Scope): Allocator =
      SegmentAllocator.newNativeArena(size, scope).nn
    def arenaAllocator(scope: Scope): Allocator =
      SegmentAllocator.newNativeArena(scope).nn
    def nativeAllocator(scope: Scope): Allocator =
      SegmentAllocator.nativeAllocator(scope).nn
    def genAllocator(fn: Long => Segment): Allocator = new SegmentAllocator:
      def allocate(size: Long, alignment: Long) = fn(size)

    def genAllocator(fn: (Long, Long) => Segment): Allocator =
      new SegmentAllocator:
        def allocate(size: Long, alignment: Long) = fn(size, alignment)

  extension (s: Allocator)
    def allocateSegment(byteSize: Long): Segment = s.allocate(byteSize).nn
    def allocateSegment(layout: Layout): Segment = s.allocate(layout).nn
    def allocate(byteSize: Long): Address = s.allocate(byteSize).nn.address.nn
    def allocate(layout: Layout): Address = s.allocate(layout).nn.address.nn
    def allocate(layout: CChar, value: Byte): Address =
      s.allocate(layout, value).nn.address.nn
    def allocate(layout: CDouble, value: Double): Address =
      s.allocate(layout, value).nn.address.nn
    def allocate(layout: CFloat, value: Float): Address =
      s.allocate(layout, value).nn.address.nn
    def allocate(layout: CInt, value: Int): Address =
      s.allocate(layout, value).nn.address.nn
    def allocate(layout: CLong, value: Long): Address =
      s.allocate(layout, value).nn.address.nn
    def allocate(layout: CPointer, value: Address): Address =
      s.allocate(layout, value).nn.address.nn
    def allocate(layout: CShort, value: Short): Address =
      s.allocate(layout, value).nn.address.nn
    def allocateArray(layout: Layout, count: Long): Address =
      s.allocateArray(layout, count).nn.address.nn
    def allocateArray(layout: CChar, array: Array[Byte]): Address =
      s.allocateArray(layout, array).nn.address.nn
    def allocateArray(layout: CDouble, array: Array[Double]): Address =
      s.allocateArray(layout, array).nn.address.nn
    def allocateArray(layout: CFloat, array: Array[Float]): Address =
      s.allocateArray(layout, array).nn.address.nn
    def allocateArray(layout: CInt, array: Array[Int]): Address =
      s.allocateArray(layout, array).nn.address.nn
    def allocateArray(layout: CLong, array: Array[Long]) =
      s.allocateArray(layout, array).nn.address.nn
    def allocateArray(layout: CShort, array: Array[Short]): Address =
      s.allocateArray(layout, array).nn.address.nn
    def allocateUtf8String(string: String): Address =
      s.allocateUtf8String(string).nn.address.nn
