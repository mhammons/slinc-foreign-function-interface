package io.gitlab.mhammons.slincffi

class FFI18
    extends FFI,
      Address18,
      Allocator18,
      CLinker18,
      Descriptor18,
      Layout18,
      Scope18,
      SymbolLookup18

object FFI18:
  def unapply(versionString: String) = versionString match
    case s"18${_}" => Some(new FFI18)
    case _         => None
