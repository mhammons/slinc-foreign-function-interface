package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{FunctionDescriptor}

trait Descriptor18 extends DescriptorProto, Layout18:
  type Descriptor = FunctionDescriptor

  extension (fd: Descriptor)
    def asVariadic(variadicLayouts: Layout*): Descriptor =
      fd.asVariadic(variadicLayouts*).nn

  val Descriptor = new DescriptorSingleton:
    def of(resLayout: Layout, argLayouts: Layout*): Descriptor =
      FunctionDescriptor.of(resLayout, argLayouts*).nn

    def ofVoid(argLayouts: Layout*): Descriptor =
      FunctionDescriptor.ofVoid(argLayouts*).nn
