package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{
  Addressable => JAddressable,
  MemorySegment,
  MemoryAddress,
  NativeSymbol
}
import scala.annotation.targetName

trait Address18 extends AddressProto, Scope18, Layout18:
  type Addressable = JAddressable
  type Segment = MemorySegment
  type Address = MemoryAddress
  type ForeignSymbol = NativeSymbol

  extension (address: Address)
    def setAddress(offset: Long, value: Address): Unit =
      address.set(CPointer, offset, value).nn
    def setByte(offset: Long, value: Byte): Unit =
      address.set(CChar, offset, value).nn
    def setDouble(offset: Long, value: Double): Unit =
      address.set(CDouble, offset, value).nn
    def setFloat(offset: Long, value: Float): Unit =
      address.set(CFloat, offset, value).nn
    def setInt(offset: Long, value: Int): Unit =
      address.set(CInt, offset, value).nn
    def setLong(offset: Long, value: Long): Unit =
      address.set(CLong, offset, value).nn
    def setShort(offset: Long, value: Short): Unit =
      address.set(CShort, offset, value).nn
    def getAddress(offset: Long): Address = address.get(CPointer, offset).nn
    def getByte(offset: Long): Byte = address.get(CChar, offset).nn
    def getDouble(offset: Long): Double = address.get(CDouble, offset).nn
    def getFloat(offset: Long): Float = address.get(CFloat, offset).nn
    def getInt(offset: Long): Int = address.get(CInt, offset).nn
    def getLong(offset: Long): Long = address.get(CLong, offset).nn
    def getShort(offset: Long): Short = address.get(CShort, offset).nn
    def addOffset(offset: Long): Address = address.addOffset(offset).nn
    def getUtf8String(offset: Long): String = address.getUtf8String(offset).nn
    def asForeignSymbol: ForeignSymbol =
      NativeSymbol.ofAddress("", address, Scope.globalScope).nn

  extension (segment: Segment)
    def address = segment.address.nn
    def asSlice(offset: Long, size: Long): Segment =
      segment.asSlice(offset, size).nn
    def copyFrom(toCopyFrom: Segment) = segment.copyFrom(toCopyFrom).nn
    def toByteArray = segment.toArray(CChar).nn
    def toDoubleArray = segment.toArray(CDouble).nn
    def toFloatArray = segment.toArray(CFloat).nn
    def toIntArray = segment.toArray(CInt).nn
    def toLongArray = segment.toArray(CLong).nn
    def toShortArray = segment.toArray(CShort).nn
    @targetName("segmentByteSize")
    def byteSize = segment.byteSize

  val Address = new AddressSingleton:
    val NULL = MemoryAddress.NULL.nn

  val Segment = new SegmentSingleton:
    def ofArray(arr: Array[Byte]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Char]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Double]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Float]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Int]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Long]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Short]) = MemorySegment.ofArray(arr).nn
    def ofAddress(addr: Address, bytesSize: Long, scope: Scope): Segment =
      MemorySegment.ofAddress(addr, bytesSize, scope).nn

  extension (addressable: Addressable)
    def address: Address = addressable.address.nn
