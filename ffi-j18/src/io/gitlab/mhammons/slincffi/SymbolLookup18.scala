package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.SymbolLookup
import scala.jdk.OptionConverters.*

trait SymbolLookup18 extends SymbolLookupProto, Address18, CLinker18:
  type Lookup = SymbolLookup

  val Lookup = new LookupSingleton:
    def loaderLookup: Lookup = SymbolLookup.loaderLookup.nn
    def systemLookup: Lookup = Linker.systemCLinker()

  extension (sL: Lookup)
    def lookup(name: String): Option[ForeignSymbol] = sL.lookup(name).nn.toScala
