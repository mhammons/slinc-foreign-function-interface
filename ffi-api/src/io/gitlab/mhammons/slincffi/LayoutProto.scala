package io.gitlab.mhammons.slincffi

import scala.reflect.TypeTest

trait LayoutProto:
  type Layout
  given typeTestOfLayout: TypeTest[Layout, Layout]
  trait LayoutSingleton:
    type Group <: Layout
    type Value <: Layout
    type Sequence <: Layout
    type Path

    given typeTestOfGroup: TypeTest[Layout, Group]
    given typeTestOfValue: TypeTest[Layout, Value]
    given typeTestOfSequence: TypeTest[Layout, Sequence]

    trait PathSingleton:
      def groupElement(name: String): Path
      def sequenceElement(index: Long): Path

    val Path: PathSingleton

    def sequenceLayout(length: Long, layout: Layout): Sequence
    def structLayout(layout: Layout*): Group

  val Layout: LayoutSingleton

  type CChar <: Layout.Value
  type CDouble <: Layout.Value
  type CFloat <: Layout.Value
  type CInt <: Layout.Value
  type CLong <: Layout.Value
  type CPointer <: Layout.Value
  type CShort <: Layout.Value

  given typeTestOfCChar: TypeTest[Layout.Value, CChar]
  given typeTestOfCDouble: TypeTest[Layout.Value, CDouble]
  given typeTestOfCFloat: TypeTest[Layout.Value, CFloat]
  given typeTestOfCInt: TypeTest[Layout.Value, CInt]
  given typeTestOfCLong: TypeTest[Layout.Value, CLong]
  given typeTestOfCPointer: TypeTest[Layout.Value, CPointer]
  given typeTestOfCShort: TypeTest[Layout.Value, CShort]

  val CChar: CChar
  val CDouble: CDouble
  val CFloat: CFloat
  val CInt: CInt
  val CLong: CLong
  val CPointer: CPointer
  val CShort: CShort

  extension [A <: Layout](vl: A)
    def withName(name: String): A
    def byteSize: Long
    def byteOffset(pathElements: Layout.Path*): Long
    def name: Option[String]

  extension (gl: Layout.Group) def memberLayouts: List[Layout]
