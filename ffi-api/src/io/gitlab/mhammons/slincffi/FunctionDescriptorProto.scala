package io.gitlab.mhammons.slincffi

trait DescriptorProto extends LayoutProto:
  type Descriptor

  extension (fd: Descriptor)
    def asVariadic(variadicLayouts: Layout*): Descriptor

  trait DescriptorSingleton:
    def of(resLayout: Layout, argLayouts: Layout*): Descriptor
    def ofVoid(argLayouts: Layout*): Descriptor

  val Descriptor: DescriptorSingleton
