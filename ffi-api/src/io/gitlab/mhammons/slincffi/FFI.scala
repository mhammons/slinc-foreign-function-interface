package io.gitlab.mhammons.slincffi

trait FFI
    extends LayoutProto,
      AddressProto,
      SymbolLookupProto,
      ScopeProto,
      AllocatorProto,
      DescriptorProto,
      CLinkerProto
