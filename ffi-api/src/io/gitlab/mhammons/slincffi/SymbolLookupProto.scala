package io.gitlab.mhammons.slincffi

trait SymbolLookupProto extends AddressProto:
  type Lookup

  trait LookupSingleton:
    def loaderLookup: Lookup
    def systemLookup: Lookup

  val Lookup: LookupSingleton

  extension (sL: Lookup) def lookup(name: String): Option[ForeignSymbol]
