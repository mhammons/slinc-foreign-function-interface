package io.gitlab.mhammons.slincffi

import scala.annotation.targetName

trait AddressProto extends LayoutProto, ScopeProto:
  type Addressable
  type Segment <: Addressable
  type Address <: Addressable
  type ForeignSymbol <: Addressable
  extension (address: Address)
    def setAddress(offset: Long, value: Address): Unit

    def setByte(offset: Long, value: Byte): Unit
    def setDouble(offset: Long, value: Double): Unit
    def setFloat(offset: Long, value: Float): Unit
    def setInt(offset: Long, value: Int): Unit
    def setLong(offset: Long, value: Long): Unit
    def setShort(offset: Long, value: Short): Unit

    def getAddress(offset: Long): Address
    def getByte(offset: Long): Byte
    def getDouble(offset: Long): Double
    def getFloat(offset: Long): Float
    def getInt(offset: Long): Int
    def getLong(offset: Long): Long
    def getShort(offset: Long): Short
    def addOffset(offset: Long): Address
    def getUtf8String(offset: Long): String

    def asForeignSymbol: ForeignSymbol

  extension (segment: Segment)
    @targetName("segmentByteSize") def byteSize: Long
    def asSlice(offset: Long, size: Long): Segment
    def copyFrom(toCopyFrom: Segment): Unit
    def toByteArray: Array[Byte]
    def toDoubleArray: Array[Double]
    def toFloatArray: Array[Float]
    def toIntArray: Array[Int]
    def toLongArray: Array[Long]
    def toShortArray: Array[Short]

  extension (addressable: Addressable) def address: Address

  trait AddressSingleton:
    val NULL: Address

  val Address: AddressSingleton

  trait SegmentSingleton:
    def ofArray(arr: Array[Byte]): Segment
    def ofArray(arr: Array[Char]): Segment
    def ofArray(arr: Array[Double]): Segment
    def ofArray(arr: Array[Float]): Segment
    def ofArray(arr: Array[Int]): Segment
    def ofArray(arr: Array[Long]): Segment
    def ofArray(arr: Array[Short]): Segment
    def ofAddress(addr: Address, bytesSize: Long, scope: Scope): Segment

  val Segment: SegmentSingleton
