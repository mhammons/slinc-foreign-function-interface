package io.gitlab.mhammons.slincffi

trait AllocatorProto extends LayoutProto, AddressProto, ScopeProto:
  type Allocator

  trait AllocatorSingleton:
    def arenaAllocator(size: Long, scope: Scope): Allocator
    def arenaAllocator(scope: Scope): Allocator
    def nativeAllocator(scope: Scope): Allocator
    def genAllocator(fn: (Long) => Segment): Allocator
    def genAllocator(fn: (Long, Long) => Segment): Allocator

  val Allocator: AllocatorSingleton

  extension (s: Allocator)
    def allocateSegment(byteSize: Long): Segment
    def allocate(byteSize: Long): Address
    def allocate(layout: Layout): Address
    def allocateSegment(layout: Layout): Segment
    def allocate(layout: CChar, value: Byte): Address
    def allocate(layout: CDouble, value: Double): Address
    def allocate(layout: CFloat, value: Float): Address
    def allocate(layout: CInt, value: Int): Address
    def allocate(layout: CLong, value: Long): Address
    def allocate(layout: CShort, value: Short): Address
    def allocate(layout: CPointer, value: Address): Address
    def allocateArray(layout: Layout, count: Long): Address
    def allocateArray(layout: CChar, array: Array[Byte]): Address
    def allocateArray(layout: CDouble, array: Array[Double]): Address
    def allocateArray(layout: CFloat, array: Array[Float]): Address
    def allocateArray(layout: CInt, array: Array[Int]): Address
    def allocateArray(layout: CLong, array: Array[Long]): Address
    def allocateArray(layout: CShort, array: Array[Short]): Address
    def allocateUtf8String(string: String): Address
