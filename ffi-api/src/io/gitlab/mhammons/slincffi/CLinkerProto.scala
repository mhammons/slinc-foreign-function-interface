package io.gitlab.mhammons.slincffi

import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodType

trait CLinkerProto extends DescriptorProto, AddressProto, AllocatorProto:
  type Linker

  extension (l: Linker)
    def downcallHandle(
        symbol: ForeignSymbol,
        functionDescriptor: Descriptor,
        allocator: Allocator
    ): MethodHandle
    def upcallStub(
        target: MethodHandle,
        functionDescriptor: Descriptor,
        scope: Scope
    ): ForeignSymbol

  trait LinkerSingleton:
    def systemCLinker(): Linker
    def downcallType(functionDescriptor: Descriptor): MethodType
    def upcallType(functionDescriptor: Descriptor): MethodType

  val Linker: LinkerSingleton
