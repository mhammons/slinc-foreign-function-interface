package io.gitlab.mhammons.slincffi

trait ScopeProto:
  type Scope

  trait ScopeSingleton:
    val globalScope: Scope
    def newConfinedScope(): Scope
    def newSharedScope(): Scope

  val Scope: ScopeSingleton

  extension (rs: Scope) def close(): Unit
