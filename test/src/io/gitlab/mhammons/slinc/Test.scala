package io.gitlab.mhammons.slinc

import io.gitlab.mhammons.slincffi.*

val ffi: FFI = System.getProperty("java.version") match 
   case FFI17(ffiRes) => ffiRes
   case _ => ???
   //case FFI18(ffiRes) => ???
