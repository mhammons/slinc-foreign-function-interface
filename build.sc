import mill._, scalalib._, scalafmt._
import $file.publishable
import de.tobiasroeser.mill.vcs.version.VcsVersion

object `ffi-api` extends ScalaModule with publishable.PublishableModule with ScalafmtModule {
  def scalaVersion = "3.1.1"
  def pomSettings = pomTemplate("Slinc-ffi-api")

  def scalacOptions = Seq(
    "-deprecation",
    "-Wunused:all",
    "-unchecked",
    "-Xcheck-macros",
    "-Xprint-suspension",
    "-Xsemanticdb",
    "-Yexplicit-nulls",
    "-Ysafe-init"
  )
}

object `ffi-j17` extends ScalaModule with publishable.PublishableModule with ScalafmtModule {
  def scalaVersion = "3.1.1"
  def pomSettings = pomTemplate("Slinc-ffi-j17")
  def moduleDeps = Seq(`ffi-api`)

  def scalacOptions = Seq(
    "-deprecation",
    "-Wunused:all",
    "-unchecked",
    "-Xcheck-macros",
    "-Xprint-suspension",
    "-Xsemanticdb",
    "-Yexplicit-nulls",
    "-Ysafe-init"
  )

  object test extends Tests with TestModule.Munit {
    def forkArgs = Seq(
      "--add-modules",
      "jdk.incubator.foreign",
      "--enable-native-access",
      "ALL-UNNAMED"
    )

    def ivyDeps = Agg(ivy"org.scalameta::munit:1.0.0-M1")
  }
}

object `ffi-j18` extends ScalaModule with publishable.PublishableModule with ScalafmtModule {
  def scalaVersion = "3.1.1"

  def pomSettings = pomTemplate("Slinc-ffi-j18")
  def moduleDeps = Seq(`ffi-api`)

  def scalacOptions = Seq(
    "-deprecation",
    "-Wunused:all",
    "-unchecked",
    "-Xcheck-macros",
    "-Xprint-suspension",
    "-Xsemanticdb",
    "-Yexplicit-nulls",
    "-Ysafe-init"
  )
}