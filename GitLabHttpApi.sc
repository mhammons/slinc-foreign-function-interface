import requests.BaseSession
import mill.BuildInfo

class GenericHTTPApi(
   uri: String,
   credentials: String,
   readTimeout: Int, 
   connectTimeout: Int
) {
   val http = requests.Session(
      readTimeout = readTimeout,
      connectTimeout = connectTimeout,
      maxRedirects = 0,
      check = false
   )

   private val commonHeaders = Seq(
      "PRIVATE_TOKEN" -> 
   )
}