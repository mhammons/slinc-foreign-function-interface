package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.ResourceScope

trait Scope17 extends ScopeProto:
  type Scope = ResourceScope

  val Scope = new ScopeSingleton:
    val globalScope: Scope = ResourceScope.globalScope.nn
    def newConfinedScope(): Scope = ResourceScope.newConfinedScope().nn
    def newSharedScope(): Scope = ResourceScope.newSharedScope().nn

  extension (rs: Scope) def close(): Unit = rs.close()
