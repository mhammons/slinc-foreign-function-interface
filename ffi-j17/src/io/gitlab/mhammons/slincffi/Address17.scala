package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{
  Addressable => JAddressable,
  MemorySegment,
  MemoryAddress,
  SymbolLookup,
  CLinker,
  MemoryAccess
}
import scala.jdk.OptionConverters.*
import scala.annotation.targetName

trait Address17 extends AddressProto, Scope17:
  type Addressable = JAddressable
  type Segment = MemorySegment
  type Address = MemoryAddress
  type ForeignSymbol = MemoryAddress

  extension (address: Address)
    def setAddress(offset: Long, value: Address): Unit = MemoryAccess
      .setAddressAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn
    def setByte(offset: Long, value: Byte): Unit = MemoryAccess
      .setByteAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn
    def setDouble(offset: Long, value: Double): Unit = MemoryAccess
      .setDoubleAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn
    def setFloat(offset: Long, value: Float): Unit = MemoryAccess
      .setFloatAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn
    def setInt(offset: Long, value: Int): Unit = MemoryAccess
      .setIntAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn
    def setLong(offset: Long, value: Long): Unit = MemoryAccess
      .setLongAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn
    def setShort(offset: Long, value: Short): Unit = MemoryAccess
      .setShortAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset,
        value
      )
      .nn

    def getAddress(offset: Long): Address = MemoryAccess
      .getAddressAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn
    def getByte(offset: Long): Byte = MemoryAccess
      .getByteAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn
    def getDouble(offset: Long): Double = MemoryAccess
      .getDoubleAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn
    def getFloat(offset: Long): Float = MemoryAccess
      .getFloatAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn
    def getInt(offset: Long): Int = MemoryAccess
      .getIntAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn
    def getLong(offset: Long): Long = MemoryAccess
      .getLongAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn
    def getShort(offset: Long): Short = MemoryAccess
      .getShortAtOffset(
        MemorySegment.globalNativeSegment,
        address.toRawLongValue + offset
      )
      .nn

    def addOffset(offset: Long): Address = address.addOffset(offset).nn
    def getUtf8String(offset: Long): String =
      CLinker.toJavaString(address.addOffset(offset)).nn

    def asForeignSymbol: ForeignSymbol = address

  extension (segment: Segment)
    def address = segment.address.nn
    def asSlice(offset: Long, size: Long): Segment =
      segment.asSlice(offset, size).nn
    def copyFrom(toCopyFrom: Segment) = segment.copyFrom(toCopyFrom).nn
    def toByteArray = segment.toByteArray.nn
    def toDoubleArray = segment.toDoubleArray.nn
    def toFloatArray = segment.toFloatArray.nn
    def toIntArray = segment.toIntArray.nn
    def toLongArray = segment.toLongArray.nn
    def toShortArray = segment.toShortArray.nn
    @targetName("segmentByteSize")
    def byteSize = segment.byteSize

  val Address = new AddressSingleton:
    val NULL = MemoryAddress.NULL.nn

  val Segment = new SegmentSingleton:
    def ofArray(arr: Array[Byte]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Char]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Double]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Float]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Int]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Long]) = MemorySegment.ofArray(arr).nn
    def ofArray(arr: Array[Short]) = MemorySegment.ofArray(arr).nn
    def ofAddress(addr: Address, bytesSize: Long, scope: Scope): Segment =
      addr.asSegment(bytesSize, scope).nn

  extension (addressable: Addressable)
    def address: Address = addressable.address.nn
