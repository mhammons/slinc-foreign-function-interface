package io.gitlab.mhammons.slincffi

class FFI17
    extends FFI,
      Address17,
      Allocator17,
      Clinker17,
      Descriptor17,
      Layout17,
      Scope17,
      SymbolLookup17

object FFI17:
  def unapply(versionString: String): Option[FFI17] = versionString match
    case s"17${_}" => Some(new FFI17)
    case _         => None
