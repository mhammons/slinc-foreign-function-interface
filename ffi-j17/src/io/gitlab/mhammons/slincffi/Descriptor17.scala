package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{FunctionDescriptor, CLinker}

trait Descriptor17 extends DescriptorProto, Layout17:
  type Descriptor = FunctionDescriptor

  extension (fd: Descriptor)
    def asVariadic(variadicLayouts: Layout*): Descriptor =
      fd.withAppendedArgumentLayouts(variadicLayouts.map(CLinker.asVarArg)*).nn

  val Descriptor = new DescriptorSingleton:
    def of(resLayout: Layout, argLayouts: Layout*): Descriptor =
      FunctionDescriptor.of(resLayout, argLayouts*).nn
    def ofVoid(argLayouts: Layout*): Descriptor =
      FunctionDescriptor.ofVoid(argLayouts*).nn
