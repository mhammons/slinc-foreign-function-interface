package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{
  MemoryLayout,
  GroupLayout,
  ValueLayout,
  SequenceLayout,
  CLinker
}
import scala.jdk.OptionConverters.*
import scala.jdk.CollectionConverters.*
import scala.reflect.TypeTest

trait Layout17 extends LayoutProto:
  type Layout = MemoryLayout
  given typeTestOfLayout: TypeTest[Layout, Layout] = TypeTest.identity[Layout]

  class Layout17Singleton extends LayoutSingleton:
    type Group = GroupLayout
    type Value = ValueLayout

    type Sequence = SequenceLayout

    given typeTestOfGroup: TypeTest[Layout, Group] with
      def unapply(x: MemoryLayout): Option[x.type & GroupLayout] =
        x match
          case g: (x.type & Group) => Some(g)
          case _                   => None

    given typeTestOfValue: TypeTest[Layout, Value] = (x: MemoryLayout) =>
      x match
        case g: (x.type & Value) => Some(g)
        case _                   => None

    given typeTestOfSequence: TypeTest[Layout, Sequence] = (x: MemoryLayout) =>
      x match
        case g: (x.type & Sequence) => Some(g)
        case _                      => None

    type Path = MemoryLayout.PathElement

    class Path17Singleton extends PathSingleton:
      def groupElement(name: String): Path =
        MemoryLayout.PathElement.groupElement(name).nn

      def sequenceElement(index: Long): Path =
        MemoryLayout.PathElement.sequenceElement(index).nn

    val layoutType = "LayoutType"

    val Path: Path17Singleton = new Path17Singleton

    def sequenceLayout(size: Long, layout: Layout): Sequence =
      MemoryLayout.sequenceLayout(size, layout).nn
    def structLayout(layouts: Layout*): Group =
      MemoryLayout.structLayout(layouts*).nn

  val Layout: Layout17Singleton = new Layout17Singleton

  type CChar = Layout.Value
  type CDouble = Layout.Value
  type CFloat = Layout.Value
  type CInt = Layout.Value
  type CLong = Layout.Value
  type CPointer = Layout.Value
  type CShort = Layout.Value


  val CChar: CChar = CLinker.C_CHAR.nn.withAttribute("LayoutType", "CChar").nn
  val CDouble: CDouble =
    CLinker.C_DOUBLE.nn.withAttribute("LayoutType", "CDouble").nn
  val CFloat: CFloat =
    CLinker.C_FLOAT.nn.withAttribute("LayoutType", "CFloat").nn
  val CInt: CInt = CLinker.C_INT.nn.withAttribute("LayoutType", "CInt").nn
  val CLong: CLong = CLinker.C_LONG.nn.withAttribute("LayoutType", "CLong").nn
  val CPointer: CPointer =
    CLinker.C_POINTER.nn.withAttribute("LayoutType", "CPointer").nn
  val CShort: CShort =
    CLinker.C_SHORT.nn.withAttribute("LayoutType", "CShort").nn

  extension (l: Layout) 
    def getAttribute(s: String) = l.attribute(s).nn.toScala

  given typeTestOfCChar: TypeTest[Layout.Value, CChar] = l =>
    l match
      case c: l.type if c.getAttribute("LayoutType").contains("CChar") => Some(c)
      case _                   => None

  given typeTestOfCDouble: TypeTest[Layout.Value, CDouble] = l =>
    l match
      case d: l.type if d.getAttribute("LayoutType").contains("CDouble") => Some(d)
      case _                     => None

  given typeTestOfCFloat: TypeTest[Layout.Value, CFloat] = l =>
    l match 
      case f: l.type if f.getAttribute("LayoutType").contains("CFloat") => Some(f)
      case _ => None

  given typeTestOfCInt: TypeTest[Layout.Value, CInt] = l =>
    l match 
      case i: l.type if i.getAttribute("LayoutType").contains("CInt") => Some(i)
      case _ => None

  given typeTestOfCLong: TypeTest[Layout.Value, CLong] = l =>
    l match 
      case i: l.type if i.getAttribute("LayoutType").contains("CLong") => Some(l)
      case _ => None

  given typeTestOfCPointer: TypeTest[Layout.Value, CPointer] = l =>
    l match 
      case p: l.type if p.getAttribute("LayoutType").contains("CPointer") => Some(p)
      case _ => None

  given typeTestOfCShort: TypeTest[Layout.Value, CShort] = l =>
    l match 
      case s: l.type if s.getAttribute("LayoutType").contains("CShort") => Some(s)
      case _ => None

  extension [A <: Layout](vl: A)
    def withName(name: String): A = vl.withName(name).nn.asInstanceOf[A]
    def byteSize =
      vl.byteSize()

    def byteOffset(pathElements: Layout.Path*): Long =
      vl.byteOffset(pathElements*).nn
    def name: Option[String] = vl.name.nn.toScala

  extension (gl: GroupLayout)
    def memberLayouts: List[Layout] = gl.memberLayouts.nn.asScala.toList
