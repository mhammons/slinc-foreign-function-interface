package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{
  CLinker,
  MemoryLayout,
  ValueLayout,
  MemoryAddress,
  MemorySegment,
  GroupLayout
}
import java.lang.invoke.MethodHandle
import java.lang.invoke.MethodType
import scala.jdk.OptionConverters.*
import scala.jdk.CollectionConverters.*

trait Clinker17 extends CLinkerProto, Descriptor17, Address17, Allocator17:
  type Linker = CLinker

  extension (l: Linker)
    def downcallHandle(
        symbol: ForeignSymbol,
        descriptor: Descriptor,
        allocator: Allocator
    ): MethodHandle =
      l.downcallHandle(
        symbol,
        allocator,
        Linker.downcallType(descriptor),
        descriptor
      ).nn
    def upcallStub(
        target: MethodHandle,
        descriptor: Descriptor,
        scope: Scope
    ): ForeignSymbol = l.upcallStub(target, descriptor, scope).nn

  val Linker = new LinkerSingleton:
    def systemCLinker(): Linker = CLinker.getInstance().nn
    private def layoutToCarrierType(ml: MemoryLayout): Class[?] = ml match
      case vl: ValueLayout =>
        vl.attribute(Layout.layoutType).nn.toScala match
          case Some("CChar")   => classOf[Byte]
          case Some("CDouble") => classOf[Double]
          case Some("CFloat")  => classOf[Float]
          case Some("CInt")    => classOf[Int]
          case Some("CLong")   => classOf[Long]
          case Some("CPointer") =>
            classOf[MemoryAddress]
          case Some("CShort") => classOf[Short]
          case _              => ???
      case gl: GroupLayout =>
        classOf[MemorySegment]
      case _ => ???

    def downcallType(descriptor: Descriptor): MethodType = (
      descriptor.returnLayout.nn.toScala,
      descriptor.argumentLayouts.nn.asScala
    ) match
      case (None, args) if args.nonEmpty =>
        val translated = args.map(layoutToCarrierType)
        VoidHelper.methodTypeV(translated.head, translated.tail.toSeq*).nn
      case (None, _) =>
        VoidHelper.methodTypeV.nn
      case (Some(r), args) if args.nonEmpty =>
        val translated = args.map(layoutToCarrierType)
        MethodType
          .methodType(
            layoutToCarrierType(r),
            translated.head,
            translated.tail.toSeq*
          )
          .nn
      case (Some(r), _) =>
        MethodType.methodType(layoutToCarrierType(r)).nn

    def upcallType(descriptor: Descriptor): MethodType =
      downcallType(descriptor)
