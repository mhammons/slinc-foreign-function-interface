package io.gitlab.mhammons.slincffi

import jdk.incubator.foreign.{SymbolLookup, CLinker}
import scala.jdk.OptionConverters.*

trait SymbolLookup17 extends SymbolLookupProto, Address17:
  type Lookup = SymbolLookup

  val Lookup = new LookupSingleton:
    def loaderLookup: Lookup = SymbolLookup.loaderLookup.nn
    def systemLookup: Lookup = CLinker.systemLookup.nn

  extension (sL: Lookup)
    def lookup(name: String): Option[ForeignSymbol] = sL.lookup(name).nn.toScala
