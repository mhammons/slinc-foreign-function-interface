package io.gitlab.mhammons.slinc

import ffi.*

class SymbolLookupSpec extends munit.FunSuite {

  test("Can summon symbol") {
    assert(Lookup.systemLookup.lookup("printf").isDefined)
  }
}
