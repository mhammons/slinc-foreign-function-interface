package io.gitlab.mhammons.slinc
import ffi.*

class MemorySpec extends munit.FunSuite:
  val allocator = Allocator.arenaAllocator(Scope.globalScope)

  test("global scope cannot be closed".fail) {
    Scope.globalScope.close()
  }

  test("can create an allocator") {
    Allocator.arenaAllocator(Scope.globalScope)
  }

  test("can allocate bytes") {
    allocator.allocate(CChar, 3.toByte)
  }

  test("can allocate shorts") {
    allocator.allocate(CShort, 2.toShort)
  }

  test("can allocate ints") {
    allocator.allocate(CInt, 3)
  }

  test("can allocate longs") {
    allocator.allocate(CLong, 5L)
  }

  test("can allocate float") {
    allocator.allocate(CFloat, 3f)
  }

  test("can allocate double") {
    allocator.allocate(CDouble, 3d)
  }

  test("can set and retrieve bytes") {
    val addr = allocator.allocate(CChar, 4.toByte)
    assertEquals(addr.getByte(0), 4.toByte)
    addr.setByte(0, 10.toByte)
    assertEquals(addr.getByte(0), 10.toByte)
  }

  test("can set and retrieve shorts") {
    val addr = allocator.allocate(CShort, 4.toShort)
    assertEquals(addr.getShort(0), 4.toShort)
    addr.setShort(0, 10.toShort)
    assertEquals(addr.getShort(0), 10.toShort)
  }

  test("can set and retrieve ints") {
    val addr = allocator.allocate(CInt, 4)
    assertEquals(addr.getInt(0), 4)
    addr.setInt(0, 10)
    assertEquals(addr.getInt(0), 10.toInt)
  }

  test("can set and retrieve longs") {
    val addr = allocator.allocate(CLong, 4L)
    assertEquals(addr.getLong(0), 4L)
    addr.setLong(0, 10)
    assertEquals(addr.getLong(0), 10.toLong)
  }

  test("can set and retrieve floats") {
    val addr = allocator.allocate(CFloat, 4f)
    assertEquals(addr.getFloat(0), 4f)
    addr.setFloat(0, 10f)
    assertEquals(addr.getFloat(0), 10f)
  }

  test("can set and retrieve doubles") {
    val addr = allocator.allocate(CDouble, 4d)
    assertEquals(addr.getDouble(0), 4d)
    addr.setDouble(0, 10d)
    assertEquals(addr.getDouble(0), 10d)
  }
