package io.gitlab.mhammons.slinc

import io.gitlab.mhammons.slincffi.FFI
import io.gitlab.mhammons.slincffi.FFI17

val ffi: FFI = System.getProperty("java.version") match
  case FFI17(ffi) => ffi
  case _          => ???
